#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "litmus.h"
#include "common.h"

static void usage(char *error) {
	fprintf(stderr, "Error: %s\n", error);
	fprintf(stderr,
		"Usage:\n"
		"	qps_add_set PARTITION SET RATE_A RATE_B\n"
		"\n");
	exit(EXIT_FAILURE);
}

int main(int argc, char** argv)
{
	int ret = 0;
	int partition, set;
	lt_t rate_a, rate_b;

	if (argc < 5)
		usage("Arguments missing.");

	partition = atoi(argv[1]);
	set = atoi(argv[2]);
	rate_a = atoi(argv[3]);
	rate_b = atoi(argv[4]);

	printf("qps_add_set %d %d %llu %llu\n", partition, set, rate_a, rate_b);

	ret = qps_add_set(partition, set, rate_a, rate_b);
	if (ret < 0)
		bail_out("Could not add the new set.");

	return 0;
}
